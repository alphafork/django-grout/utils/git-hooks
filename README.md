# Grout Git Hooks

Centralized git hooks using
[pre-commit](https://github.com/pre-commit/pre-commit) for the [grout
repos](https://gitlab.com/alphafork/django-grout).


## Usage

To use the hooks defined in the `.pre-commit-hooks.yaml` file of this repo,
add the following to the `.pre-commit-config.yaml` file in the consuimg repo:
```
repos:
   - repo: https://gitlab.com/alphafork/django-grout/utils/git-hooks
     rev: 0.1.0
     hooks:
     - id: grout-hook

```
For more details, see this [issue](https://github.com/pre-commit/pre-commit/issues/731#issuecomment-376945745).


## License

[GPL-3.0-or-later](LICENSE)


## Contact

[Alpha Fork Technologies](https://alphafork.com/)

Email: [connect@alphafork.com](mailto:connect@alphafork.com)
